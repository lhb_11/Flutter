import 'package:flutter/material.dart';

/// MaterialApp 一个android 的app管理组件
/// 1.navigatorKey.currentState 相当于 Navigator.of(context)
///
///
///

class ListViewDev extends StatefulWidget {
  ListViewDev({Key key, this.defalutTitle: ''}) : super(key: key);
  final String defalutTitle;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListViewDevState();
  }
}

class ListViewDevState extends State<ListViewDev> {
  final items = [
    'ListView 组件',
    'ListView.builder 组件',
    'ListView.separated 组件',
    'ListView.custom 组件'
  ];
  final describe = ['包含主题和home页面的组件', '列表组件', '暂定', '暂定'];
  final List<Icon> iconItems = <Icon>[
    new Icon(Icons.keyboard),
    new Icon(Icons.print),
    new Icon(Icons.router),
    new Icon(Icons.pages),
    new Icon(Icons.zoom_out_map),
    new Icon(Icons.zoom_out),
    new Icon(Icons.youtube_searched_for),
    new Icon(Icons.wifi_tethering),
    new Icon(Icons.wifi_lock),
    new Icon(Icons.widgets),
    new Icon(Icons.weekend),
    new Icon(Icons.web),
    new Icon(Icons.accessible),
    new Icon(Icons.ac_unit),
  ];
  final List<Widget> _list = new List();
  var index = 0;
  void initListArray(BuildContext context) {
    // 初始化列表数据解构
    for (int i = 0; i < items.length; i++) {
      _list.add(buildListData(context, items[i], iconItems[i]));
    }
  }

  Widget buildListData(BuildContext context, String titleItem, Icon iconItem) {
    return new ListTile(
      isThreeLine: false,
      leading: iconItem,
      title: new Text(titleItem),
      trailing: new Icon(Icons.keyboard_arrow_right),
      onTap: () {
        setState(() {
                  index = 1;
                });
      },
      onLongPress: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return new AlertDialog(
              title: new Text(
                'ListViewDemo',
                style: new TextStyle(
                  color: Colors.black54,
                  fontSize: 18.0,
                ),
              ),
              content: new Text('您选择的item内容为 $titleItem'),
            );
          },
        );
      },
    );
  }

  Widget getCurrentWidget() {
    print(index);
    // 添加分割线
    var divideList =
        ListTile.divideTiles(context: context, tiles: _list).toList();
    switch (index) {
      case 0:
        return new Scrollbar(
          child: ListView(
            // 添加ListView控件
            //        children: _list,    // 无分割线
            children: divideList, // 添加分割线
          ),
        );
        break;
      case 1:
        return new Scrollbar(
          child: new ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, item) {
              return new Container(
                child: new Column(
                  children: <Widget>[
                    buildListData(context, items[item] + 'A', iconItems[item]),
                    new Divider()
                  ],
                ),
              );
            },
          ),
        );
        break;
      case 2:
        return new Scrollbar(
          child: new ListView.separated(
            itemCount: items.length,
            separatorBuilder: (BuildContext context, int index) =>
                new Divider(), // 添加分割线
            itemBuilder: (context, item) {
              return buildListData(context, items[item], iconItems[item]);
            },
          ),
        );
        break;
      case 3:
        return new Scrollbar(
          child: new ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, item) {
              return new Container(
                child: new Column(
                  children: <Widget>[
                    buildListData(context, items[item], iconItems[item]),
                    new Divider()
                  ],
                ),
              );
            },
          ),
        );
        break;
      default:
        return new Scrollbar(
          child: ListView(
            // 添加ListView控件
            //        children: _list,    // 无分割线
            children: divideList, // 添加分割线
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    initListArray(context);
    return Scaffold(
        appBar: AppBar(title: Text(widget.defalutTitle)),
        body: getCurrentWidget());
  }
}
