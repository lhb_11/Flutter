import 'package:flutter/material.dart';
import '../home/home_child.dart';
/*
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter_localizations/flutter_localizations.dart';
*/

void main() => runApp(MyApp());

/// MaterialApp 一个android 的app管理组件
/// 1.navigatorKey.currentState 相当于 Navigator.of(context)
/// 2.title 标题 暂时无用 (该标题出现在 Android：任务管理器的程序快照之上 IOS: 程序切换管理器中)
/// 3.theme 设置主题样式  应用程序的主题，各种的定制颜色都可以设置，用于程序主题切换   ThemeData(primarySwatch: Colors.blue, primaryColor: Colors.yellow), primaryColor影响导航背景色
/// 4.home 进入程序后显示的第一个页面,传入的是一个Widget，但实际上这个Widget需要包裹一个Scaffold以显示该程序使用Material Design风格
/// 5.routes 声明程序中有哪个通过Navigation.of(context).pushNamed跳转的路由参数以键值对的形式传递
/// 6.initialRoute 初始路由，当用户进入程序时，自动打开对应的路由。传入的是上面routes的key 跳转的是对应的Widget（如果该Widget有Scaffold.AppBar,并不做任何修改，左上角有返回键）
/// 7.onGenerateRoute 当通过Navigation.of(context).pushNamed跳转路由时，在routes查找不到时，会调用该方法
/// 8.onUnknownRoute 效果跟onGenerateRoute一样调用顺序为onGenerateRoute ==> onUnknownRoute
/// 9.navigatorObservers 路由观察器，当调用Navigator的相关方法时，会回调相关的操作
/// 10.builder 当构建一个Widget前调用一般做字体大小，方向，主题颜色等配置
/// 11.onGenerateTitle   跟上面的tiitle一样，但含有一个context参数 用于做本地化
/// 12.color  该颜色为Android中程序切换中应用图标背景的颜色，当应用图标背景为透明时
/// 13.locale 当前区域，如果为null则使用系统区域一般用于语言切换
/// 14.localizationsDelegates 本地化委托，用于更改Flutter Widget默认的提示语，按钮text等
/// 15.showSemanticsDebugger 当为true时，打开Widget边框，类似Android开发者模式中显示布局边界
/// 16.debugShowCheckedModeBanner
/// 
/// 

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();

  void pushPage() {
    _navigatorKey.currentState
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('路由二级')),
        body: Text("子页面"),
      );
    }));
  }

  void secPushPage() {
    _navigatorKey.currentState.pushNamed("/d");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      navigatorKey: _navigatorKey,
      title: 'Flutter集合',
      theme: ThemeData(primarySwatch: Colors.blue, primaryColor: Colors.red),
      home: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                onPressed: pushPage,
                child: Text("测试MaterialApp的navigatorKey功能"),
              ),
              FlatButton(
                onPressed: secPushPage,
                child: Text("测试MaterialApp的routes功能"),
                color: Colors.blue,
              )
            ],
          )
        ],
      ),
      routes: <String, WidgetBuilder>{
        '/a': (BuildContext context) => HomeChild(defalutTitle: '页面 A'),
        '/b': (BuildContext context) => HomeChild(defalutTitle: '页面 B'),
        '/c': (BuildContext context) => HomeChild(defalutTitle: '页面 C'),
      },
      initialRoute: '/c',
      onGenerateRoute: (setting) {
        print('没有找到路由1 $setting');
        //setting.isInitialRoute; bool类型 是否初始路由
        //setting.name; 要跳转的路由名key
        return new PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              //这里为返回的Widget
              return HomeChild(defalutTitle: '页面 onGenerateRoute');
            },
            opaque: false,
            //跳转动画
            transitionDuration: new Duration(milliseconds: 200),
            transitionsBuilder:
                (___, Animation<double> animation, ____, Widget child) {
              return new FadeTransition(
                opacity: animation,
                child: new ScaleTransition(
                  scale: new Tween<double>(begin: 0.5, end: 1.0)
                      .animate(animation),
                  child: child,
                ),
              );
            });
      },
      onUnknownRoute: (setting) {
        print('没有找到路由2 $setting');
      },
      navigatorObservers: [
        MyObserver(),
      ],
      builder: (BuildContext context,  Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(
            //字体大小
                textScaleFactor: 1.1,
              ),
          child: child,
        );
      },
      onGenerateTitle: (BuildContext context) {
        // print(context);
        return '标题22';
      },
      color: Colors.blue,
      //传入两个参数，语言代码，国家代码
      // locale:  Locale('yy','zh'),
      // localizationsDelegates: [
      //   MyLocalizationsDelegates()
      // ],
      debugShowMaterialGrid: false,
      showPerformanceOverlay: false,
      checkerboardRasterCacheImages: false,
      checkerboardOffscreenLayers: false,
      showSemanticsDebugger: false,
      debugShowCheckedModeBanner: false
    );
  }
}

//继承NavigatorObserver
class MyObserver extends NavigatorObserver{
  @override
  void didPush(Route route, Route previousRoute) {
    // 当调用Navigator.push时回调
    super.didPush(route, previousRoute);
    //可通过route.settings获取路由相关内容
    //route.currentResult获取返回内容
    //....等等
    print(route.settings.name);
  }
}
/*
class MyLocalizationsDelegates extends LocalizationsDelegate
<MaterialLocalizations>{
  @override
  bool isSupported(Locale locale) {
//是否支持该locale，如果不支持会报异常
    if(locale == const Locale('zh','cn')){
      return true;
    }
    return false;
  }
  @override//是否需要重载
  bool shouldReload(LocalizationsDelegate old)  => false;

  @override
  Future<MaterialLocalizations> load(Locale locale) {
//加载本地化
    //return new SynchronousFuture<MaterialLocalizations>(new MyLocalizations(locale));
    return new SynchronousFuture(new MyLocalizations(locale));
  }
}
//本地化实现，继承DefaultMaterialLocalizations
class MyLocalizations extends DefaultMaterialLocalizations{
  final Locale locale;
  MyLocalizations(this.locale, );
  @override
  String get okButtonLabel {
    if(locale == const Locale('zh','cn')){
      return '好的';
    }else{
      return super.okButtonLabel;
    }
  }
  @override
  String get backButtonTooltip {
    if(locale == const Locale('zh','cn')){
      return '返回';
    }else{
      return super.okButtonLabel;
    }
  }
}
*/