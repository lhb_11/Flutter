import 'package:flutter/material.dart';

class RouterSec extends StatefulWidget {
  RouterSec({Key key}) : super(key: key);

  @override
  _RouterSecState createState() => _RouterSecState();
}

class _RouterSecState extends State<RouterSec> {
  var cont;
  Future result;
  void backClick() {
    Navigator.pop(cont, "返回之后回传的值22");
  }

  void getBackValue() {
    result.then((value) {
      print(value);
    });
  }

  void buttonClick() {
    print("按钮事件");
    result =
        Navigator.push(cont, MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('路由'),
        ),
        body: RaisedButton(
          onPressed: backClick,
          child: Text("返回"),
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    cont = context;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.favorite),
          onPressed: buttonClick,
        ),
        RaisedButton(
          onPressed: getBackValue,
          child: Text("获取pop返回值"),
        )
      ],
    );
  }
}
