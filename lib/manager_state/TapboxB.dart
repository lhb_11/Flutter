import 'package:flutter/material.dart';

/**
 * ParentWidgetState 类:
  * 为TapboxB 管理_active状态.
    实现_handleTapboxChanged()，当盒子被点击时调用的方法.
    当状态改变时，调用setState()更新UI.

  TapboxB 类:
    继承StatelessWidget类，因为所有状态都由其父widget处理.
    当检测到点击时，它会通知父widget.
 */
class ParentWidget extends StatefulWidget {
 @override
   _ParentWidgetState createState() => new _ParentWidgetState();
}

class _ParentWidgetState extends State {
  bool _active = false;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

 @override
   Widget build(BuildContext context) {
     // TODO: implement build
     return new Container(
       child: new TapboxB(
         active: _active,
         onChanged: _handleTapboxChanged
       ),
     );
   }
}

class TapboxB extends StatelessWidget{
  // 定义构造函数，属性和默认值，@required 必须实现，的方法
  TapboxB({Key key, this.active: false, @required this.onChanged}) : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged; // 定义传参的函数

  void _handleTap() {
    onChanged(!active);
  }

  @override
    Widget build(BuildContext context) {
      return new GestureDetector(
        onTap: _handleTap,
        child: new Container(
          child: new Center(
            child: new Text(
              active ? 'Active' : 'Inactive',
              style: new TextStyle(fontSize: 32.0, color: Colors.white),
            ),
          ),
          width: 200.0,
          height: 200.0,
          decoration: new BoxDecoration(
            color: active ? Colors.orange[700] : Colors.red[600],
          ),
        ),
      );
    }
}