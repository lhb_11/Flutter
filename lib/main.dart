import 'package:flutter/material.dart';
import './home/home_child.dart';
import './widget/MaterialAppDev.dart';
import './widget/ListViewDev.dart';

void main() => runApp(MyApp());

/// 主页面 各组件的入口

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  final items = ['MaterialApp 组件', 'ListView 组件', ''];
  final describe = ['包含主题和home页面的组件', '列表组件', '暂定'];
  final List<Icon> iconItems = <Icon>[
      new Icon(Icons.keyboard), new Icon(Icons.print),
      new Icon(Icons.router), new Icon(Icons.pages),
      new Icon(Icons.zoom_out_map), new Icon(Icons.zoom_out),
      new Icon(Icons.youtube_searched_for), new Icon(Icons.wifi_tethering),
      new Icon(Icons.wifi_lock), new Icon(Icons.widgets),
      new Icon(Icons.weekend), new Icon(Icons.web),
      new Icon(Icons.accessible), new Icon(Icons.ac_unit),
  ];
  void pushPage() {
    _navigatorKey.currentState
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('路由二级')),
        body: Text("子页面"),
      );
    }));
  }

  void secPushPage() {
    _navigatorKey.currentState.pushNamed("/d");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      navigatorKey: _navigatorKey,
      title: 'Flutter集合',
      theme: ThemeData(primarySwatch: Colors.blue, primaryColor: Colors.red),
      home: Scaffold(
        appBar: AppBar(
          title: Text("入口"),
        ),
        body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
          //  if (index.isOdd) new Divider();
            return ListTile(
              title: Text(items[index]),
              subtitle: Text(describe[index]),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: (){
               switch(index) {
                 case 0:
                  _navigatorKey.currentState.pushNamed("/d");
                 break;
                 case 1:
                  _navigatorKey.currentState.pushNamed("/e");
                 break;
                 case 2:
                 break;
               }
              },
            );
          },
        ),
      ),
      routes: <String, WidgetBuilder>{
        '/a': (BuildContext context) => HomeChild(defalutTitle: '页面 A'),
        '/b': (BuildContext context) => HomeChild(defalutTitle: '页面 B'),
        '/c': (BuildContext context) => HomeChild(defalutTitle: '页面 c'),
        '/d': (BuildContext context) => MyApp(),
        '/e': (BuildContext context) => ListViewDev(defalutTitle: '列表'),
      },
      initialRoute: '/c',
      onGenerateRoute: (setting) {
        print('没有找到路由1 $setting');
        //setting.isInitialRoute; bool类型 是否初始路由
        //setting.name; 要跳转的路由名key
        return new PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
              //这里为返回的Widget
              return HomeChild(defalutTitle: '页面 onGenerateRoute');
            },
            opaque: false,
            //跳转动画
            transitionDuration: new Duration(milliseconds: 200),
            transitionsBuilder:
                (___, Animation<double> animation, ____, Widget child) {
              return new FadeTransition(
                opacity: animation,
                child: new ScaleTransition(
                  scale: new Tween<double>(begin: 0.5, end: 1.0)
                      .animate(animation),
                  child: child,
                ),
              );
            });
      },
      onUnknownRoute: (setting) {
        print('没有找到路由2 $setting');
      },
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(
            //字体大小
            textScaleFactor: 1.1,
          ),
          child: child,
        );
      },
      onGenerateTitle: (BuildContext context) {
        // print(context);
        return '标题22';
      },
      color: Colors.blue,
    );
  }
}
