import 'package:flutter/material.dart';

/// MaterialApp 一个android 的app管理组件
/// 1.navigatorKey.currentState 相当于 Navigator.of(context)
///
///
///

class HomeChild extends StatefulWidget {
 HomeChild({Key key, this.defalutTitle : ''}) : super(key: key);
 final String defalutTitle;
  @override
    State<StatefulWidget> createState() {
      // TODO: implement createState
      return HomeChildState();
    }
}

class HomeChildState extends State<HomeChild> {

 @override
   Widget build(BuildContext context) {
     // TODO: implement build
     return Scaffold(
        appBar: AppBar(
          title: Text(widget.defalutTitle)
        ),
        body: Text("首页的二级页面"),
     );
   }
}
